import { ICliente } from './cliente';

export interface Ifactura{
    idfactura: number,
    descripcion: string,
    montofinal: number,
    cliente ?: ICliente
}
export class Factura implements Ifactura{
    constructor(

        public idfactura: number,
        public descripcion: string,
        public montofinal: number,
        public  cliente ?: ICliente
        
    ){
        
    }
}