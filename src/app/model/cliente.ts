import { Ifactura } from './factura';

export interface ICliente{
    id: number,
    fullnmae: string,
    tipocliente: string,
    facturaList ?: Ifactura[]
}
export class Cliente implements ICliente {
    constructor(
        public id : number,
        public fullnmae : string,
        public tipocliente : string,
        public facturaList ?: Ifactura[]
    ){

    }
}